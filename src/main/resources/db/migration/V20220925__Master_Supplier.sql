CREATE SEQUENCE "SUPPLIER_SEQ"
INCREMENT BY 1
START WITH 101;

CREATE TABLE supplier (
	id BIGINT,
	nama VARCHAR(60),
	alamat VARCHAR(200),
	telepon VARCHAR(25),
	norek VARCHAR(30),
	bank VARCHAR(30),
	CONSTRAINT uq_norek UNIQUE(norek),
	CONSTRAINT pk_supplier PRIMARY KEY (id)
);

INSERT INTO supplier (id, nama, alamat, telepon, norek, bank) VALUES (1, 'Superindo', 'Jl. Satelit utara', '12345', '010203', 'BCA');
INSERT INTO supplier (id, nama, alamat, telepon, norek, bank) VALUES (2, 'Vanilla', 'Jl. Darmo Indah', '23456', '102030', 'Mandiri');
