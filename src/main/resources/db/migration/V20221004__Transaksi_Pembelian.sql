CREATE TABLE beli (
    id VARCHAR(36),
    status VARCHAR(20),
    tanggal DATE,
    created DATE,
    supplier_id BIGINT,
    CONSTRAINT fk_supplier_beli FOREIGN KEY(supplier_id) REFERENCES supplier(id),
    CONSTRAINT pk_beli PRIMARY KEY (id)
);

CREATE TABLE beli_dt (
    id VARCHAR(36),
    beli_id VARCHAR(36),
    barang_id BIGINT,
    qty DOUBLE,
    harga DOUBLE,
    CONSTRAINT fk_barang_beli_dt FOREIGN KEY(barang_id) REFERENCES barang(id),
    CONSTRAINT fk_hd_beli_dt FOREIGN KEY(beli_id) REFERENCES beli(id),
    CONSTRAINT pk_beli_dt PRIMARY KEY (id)
);

INSERT INTO beli (id, tanggal, supplier_id, created, status) VALUES('b86f7010-77be-443f-b590-42d955e687a5', '2022-01-01', 1, '1970-01-01', 'CLOSED');
INSERT INTO beli_dt (id, beli_id, barang_id, qty, harga) VALUES('b86f7010', 'b86f7010-77be-443f-b590-42d955e687a5', 1, 10, 2000);

INSERT INTO beli (id, tanggal, supplier_id, created, status) VALUES('994d0f85-28a5-4153-a3a3-62ce08f04488', '2022-01-02', 1, '1970-01-01', 'OPEN');
INSERT INTO beli_dt (id, beli_id, barang_id, qty, harga) VALUES('994d0f85', '994d0f85-28a5-4153-a3a3-62ce08f04488', 2, 10, 3000);
INSERT INTO beli_dt (id, beli_id, barang_id, qty, harga) VALUES('62ce08f04488', '994d0f85-28a5-4153-a3a3-62ce08f04488', 3, 5, 2000);