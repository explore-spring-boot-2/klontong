CREATE TABLE jual (
    id VARCHAR(36),
    status VARCHAR(20),
    tanggal DATE,
    created DATE,
    customer_id BIGINT,
    CONSTRAINT fk_customer_jual FOREIGN KEY(customer_id) REFERENCES customer(id),
    CONSTRAINT pk_jual PRIMARY KEY (id)
);

CREATE TABLE jual_dt (
    id VARCHAR(36),
    jual_id VARCHAR(36),
    barang_id BIGINT,
    qty DOUBLE,
    harga DOUBLE,
    CONSTRAINT fk_barang_jual_dt FOREIGN KEY(barang_id) REFERENCES barang(id),
    CONSTRAINT fk_hd_jual_dt FOREIGN KEY(jual_id) REFERENCES jual(id),
    CONSTRAINT pk_jual_dt PRIMARY KEY (id)
);

INSERT INTO jual (id, tanggal, customer_id, created, status) VALUES('e77de5b5-2ae7-44ff-a084-2e35f6e0ef81', '2022-01-01', 1, '1970-01-01', 'CLOSED');
INSERT INTO jual_dt (id, jual_id, barang_id, qty, harga) VALUES('2e35f6e0ef81', 'e77de5b5-2ae7-44ff-a084-2e35f6e0ef81', 1, 10, 2500);

INSERT INTO jual (id, tanggal, customer_id, created, status) VALUES('deaf6d3c-6c1c-4e5f-a20a-fac38c58c709', '2022-01-02', 1, '1970-01-01', 'OPEN');
INSERT INTO jual_dt (id, jual_id, barang_id, qty, harga) VALUES('deaf6d3c', 'deaf6d3c-6c1c-4e5f-a20a-fac38c58c709', 2, 10, 3500);
INSERT INTO jual_dt (id, jual_id, barang_id, qty, harga) VALUES('fac38c58c709', 'deaf6d3c-6c1c-4e5f-a20a-fac38c58c709', 3, 5, 2500);