CREATE SEQUENCE "CUSTOMER_SEQ"
INCREMENT BY 1
START WITH 101;

CREATE TABLE customer (
	id BIGINT,
	nama VARCHAR(60),
	alamat VARCHAR(200),
	telepon VARCHAR(25),
	dob DATE,
	CONSTRAINT customer PRIMARY KEY (id)
);

INSERT INTO customer (id, nama, alamat, telepon) VALUES (1, 'CASH', 'Jl. Tunai', '0812');
INSERT INTO customer (id, nama, alamat, telepon, dob) VALUES (2, 'Budi', 'Jl. Seruni', '0823', '1990-01-01');
