CREATE SEQUENCE "BARANG_SEQ"
INCREMENT BY 1
START WITH 101;

CREATE TABLE Barang (
	id BIGINT,
	nama VARCHAR(60),
	stok DOUBLE,
	CONSTRAINT pk_barang PRIMARY KEY (id)
);

INSERT INTO Barang(id, nama, stok) VALUES (1, 'Indomie Goreng', 10);
INSERT INTO Barang(id, nama, stok) VALUES (2, 'Mie Sedaap Cup Kari Special', 20);
INSERT INTO Barang(id, nama, stok) VALUES (3, 'Aqua Botol 600ml', 30);
INSERT INTO Barang(id, nama, stok) VALUES (4, 'Nescafe Classic 50gr', 40);
INSERT INTO Barang(id, nama, stok) VALUES (5, 'Mie Sedaap Singapore Spicy Laksa', 50);