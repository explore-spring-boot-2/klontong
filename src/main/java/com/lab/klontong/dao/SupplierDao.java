package com.lab.klontong.dao;

import com.lab.klontong.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface SupplierDao extends JpaRepository<Supplier, Long> {
    @Query(value = "SELECT " +
            " ID AS \"id\" " +
            " , nama AS \"text\" " +
        " FROM supplier " +
        " WHERE nama ILIKE '%' || :q || '%' "
    , nativeQuery = true)
    public List<Map<String, Object>> acSupplier(String q) ;
}
