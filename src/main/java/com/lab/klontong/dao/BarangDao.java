package com.lab.klontong.dao;

import com.lab.klontong.entity.Barang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface BarangDao extends JpaRepository<Barang, Long> {
    @Query(value = "SELECT " +
            " ID AS \"id\" " +
            " , nama AS \"text\" " +
            " FROM barang " +
            " WHERE nama ILIKE '%' || :q || '%' "
            , nativeQuery = true)
    public List<Map<String, Object>> acBarang(String q) ;

}
