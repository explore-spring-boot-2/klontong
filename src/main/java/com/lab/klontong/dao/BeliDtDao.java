package com.lab.klontong.dao;

import com.lab.klontong.entity.BeliDt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeliDtDao extends JpaRepository<BeliDt, String> {
}
