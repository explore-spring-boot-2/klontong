package com.lab.klontong.dao;

import com.lab.klontong.entity.JualDt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JualDtDao extends JpaRepository<JualDt, String> {
}
