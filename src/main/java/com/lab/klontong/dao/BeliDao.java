package com.lab.klontong.dao;

import com.lab.klontong.entity.Beli;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeliDao extends JpaRepository<Beli, String> {
}
