package com.lab.klontong.dao;

import com.lab.klontong.entity.Jual;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JualDao extends JpaRepository<Jual, String> {
}
