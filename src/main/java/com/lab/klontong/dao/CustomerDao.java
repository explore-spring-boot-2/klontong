package com.lab.klontong.dao;

import com.lab.klontong.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface CustomerDao extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT " +
            " ID AS \"id\" " +
            " , nama AS \"text\" " +
            " FROM customer " +
            " WHERE nama ILIKE '%' || :q || '%' "
            , nativeQuery = true)
    public List<Map<String, Object>> acCustomer(String q);
}
