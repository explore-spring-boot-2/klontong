package com.lab.klontong.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Data
@Entity
public class Supplier extends Vendor {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SUPPLIER_SEQ")
    @SequenceGenerator(name = "SUPPLIER_SEQ", allocationSize = 1)
    Long id;

    @NotEmpty(message = "Nomor rekening harus diisi")
    String norek;

    String bank;
}
