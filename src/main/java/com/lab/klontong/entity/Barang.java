package com.lab.klontong.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Barang {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BARANG_SEQ")
    @SequenceGenerator(name = "BARANG_SEQ", allocationSize = 1)
    Long id;

    String nama;

    Double stok;
}
