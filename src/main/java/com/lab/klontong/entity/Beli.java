package com.lab.klontong.entity;

import lombok.Data;
import net.bytebuddy.asm.Advice;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity @Data
public class Beli {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    private String id;

    @DateTimeFormat(pattern = "yyyy-MM-dd") // nanti pake viewnya input date HTML5
    private LocalDate tanggal;

    @ManyToOne
    @JoinColumn(name="supplier_id")
    private Supplier supplier;

    private Date created;

    @OneToMany(mappedBy = "beli", fetch = FetchType.EAGER)
    private List<BeliDt> dt;

    @Enumerated(EnumType.STRING)
    private Status status = Status.OPEN;

    @PrePersist
    public void beforeInsert() {
        created = new Date();
    }

    public static enum Status {
        OPEN, CLOSED, CANCELED;
    }
}

