package com.lab.klontong.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class JualDt {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    private String id;

    @ManyToOne
    @JoinColumn(name="jual_id")
    private Jual jual;

    @ManyToOne
    @JoinColumn(name = "barang_id")
    private Barang barang;

    @NotNull(message = "Qty wajib diisi")
    private Double qty;

    private Double harga;

}
