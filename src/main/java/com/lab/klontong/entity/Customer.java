package com.lab.klontong.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Customer extends Vendor {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUSTOMER_SEQ")
    @SequenceGenerator(name = "CUSTOMER_SEQ", allocationSize = 1)
    Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd") // nanti pake viewnya input date HTML5
    LocalDate dob;
}
