package com.lab.klontong.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@MappedSuperclass
public class Vendor {

    String nama;
    String alamat;
    String telepon;
}
