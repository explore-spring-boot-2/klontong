package com.lab.klontong.service;

import com.lab.klontong.dao.BarangDao;
import com.lab.klontong.dao.BeliDao;
import com.lab.klontong.entity.Barang;
import com.lab.klontong.entity.Beli;
import com.lab.klontong.entity.BeliDt;
import com.lab.klontong.exception.KlontongException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

@Service
public class BeliService {
    @Autowired
    BeliDao beliDao;
    @Autowired
    BarangDao barangDao;

    @Transactional(rollbackFor = {SQLException.class, KlontongException.class})
    public void closing(Beli beli) throws SQLException, KlontongException {
        if (beli.getStatus() != Beli.Status.OPEN) throw new KlontongException("Hanya transaksi OPEN yang dapat di CLOSING");
        for (BeliDt beliDt : beli.getDt()) {
            Barang barang = beliDt.getBarang();
            barang.setStok(barang.getStok() + beliDt.getQty());
            barangDao.save(barang);
        }
        beli.setStatus(Beli.Status.CLOSED);
    }
}
