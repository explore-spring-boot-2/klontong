package com.lab.klontong.exception;

public class KlontongException extends Exception {
    public KlontongException(String message) {
        super(message);
    }
}
