package com.lab.klontong.controller;

import com.lab.klontong.dao.BarangDao;
import com.lab.klontong.dao.CustomerDao;
import com.lab.klontong.dao.SupplierDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ac")
public class AutocompleteController {
    @Autowired
    SupplierDao supplierDao;
    @Autowired
    CustomerDao customerDao;
    @Autowired
    BarangDao barangDao;

    @GetMapping("/supplier")
    public List<Map<String, Object>> supplier(HttpServletRequest httpServletRequest) {
        String q = "";
        if (httpServletRequest.getParameter("q") != null) q = httpServletRequest.getParameter("q").toString();
        return supplierDao.acSupplier(q);
    }

    @GetMapping("/customer")
    public List<Map<String, Object>> customer(HttpServletRequest httpServletRequest) {
        String q = "";
        if (httpServletRequest.getParameter("q") != null) q = httpServletRequest.getParameter("q").toString();
        return customerDao.acCustomer(q);
    }

    @GetMapping("/barang")
    public List<Map<String, Object>> barang(HttpServletRequest httpServletRequest) {
        String q = "";
        if (httpServletRequest.getParameter("q") != null) q = httpServletRequest.getParameter("q").toString();
        return barangDao.acBarang(q);
    }
}
