package com.lab.klontong.controller;

import com.lab.klontong.dao.CustomerDao;
import com.lab.klontong.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    CustomerDao customerDao;

    @GetMapping("/")
    public String index(Model model, @SortDefault("nama") Pageable pageable){
        model.addAttribute("customers", customerDao.findAll(pageable));
        return "customer/index.html";
    }

    @GetMapping("/create")
    public String create(Customer customer) {
        return("customer/create");
    }

    @PostMapping("/save")
    public String save(HttpServletRequest request, @Valid Customer customer, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "customer/create";
        }
        customerDao.save(customer);
        return "redirect:/customer/";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Customer customer = customerDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Customer:" + id));

        model.addAttribute("customer", customer);
        return "customer/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@Valid Customer customer, BindingResult result) {
        if (result.hasErrors()) {
            return "customer/edit";
        }
        customerDao.save(customer);
        return "redirect:/customer/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model) {
        Customer customer = customerDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Customer:" + id));
        customerDao.delete(customer);
        return "redirect:/customer/";
    }

}
