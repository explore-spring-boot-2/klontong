package com.lab.klontong.controller;

import com.lab.klontong.dao.BarangDao;
import com.lab.klontong.entity.Barang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/barang")
public class BarangController {
    @Autowired
    BarangDao barangDao;

    @GetMapping("/")
    public String index(Model model, @SortDefault("nama") Pageable pageable) {
//        System.out.println("===== getPageNumber: " + pageable.getPageNumber());
//        System.out.println("===== getPageSize  : " + pageable.getPageSize());
        // example:
        //      http://localhost:8080/barang/?size=5
        //      http://localhost:8080/barang/?size=5&page=0
        model.addAttribute("barangs", barangDao.findAll(pageable));
        return "barang/index";
    }

    @GetMapping("/create")
    public String create(Barang barang) {
        return("barang/create");
    }

    @PostMapping("/save")
    public String save(HttpServletRequest request, Model model) {
        Barang barang = new Barang();
        barang.setNama(request.getParameter("nama").toString());
        barangDao.save(barang);
        return "redirect:/barang/";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Barang barang = barangDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Barang:" + id));

        model.addAttribute("barang", barang);
        return "barang/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, HttpServletRequest request, Model model) {
        Barang barang = barangDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Barang:" + id));
        barang.setNama(request.getParameter("nama").toString());
        barangDao.save(barang);
        return "redirect:/barang/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model) {
        Barang barang = barangDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Barang:" + id));
        barangDao.delete(barang);
        return "redirect:/barang/";
    }

}
