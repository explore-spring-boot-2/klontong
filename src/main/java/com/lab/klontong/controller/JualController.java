package com.lab.klontong.controller;

import com.lab.klontong.dao.JualDao;
import com.lab.klontong.entity.Beli;
import com.lab.klontong.entity.Jual;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/jual")
public class JualController {
    @Autowired
    JualDao jualDao;

    @GetMapping("/")
    public String index(Model model, @SortDefault("tanggal") Pageable pageable) {
        model.addAttribute("juals", jualDao.findAll(pageable));
        return "jual/index";
    }

    @GetMapping("/create")
    public String create(Jual jual) {
        return("jual/create");
    }

    @PostMapping("/save")
    public String save(HttpServletRequest request, @Valid Jual jual, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "jual/create";
        }
        jualDao.save(jual);
        return "redirect:/jual/";
    }

}
