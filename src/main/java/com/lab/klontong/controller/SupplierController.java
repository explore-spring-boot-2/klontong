package com.lab.klontong.controller;

import com.lab.klontong.dao.SupplierDao;
import com.lab.klontong.entity.Barang;
import com.lab.klontong.entity.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    SupplierDao supplierDao;

    @GetMapping("/")
    public String index(Model model, @SortDefault("nama")  Pageable pageable) {
        model.addAttribute("suppliers", supplierDao.findAll(pageable));
        return "supplier/index";
    }

    @GetMapping("/create")
    public String create(Supplier supplier) {
        return("supplier/create");
    }

    @PostMapping("/save")
    public String save(HttpServletRequest request, @Valid Supplier supplier, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "supplier/create";
        }
        supplierDao.save(supplier);
        return "redirect:/supplier/";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Supplier supplier = supplierDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Supplier:" + id));

        model.addAttribute("supplier", supplier);
        return "supplier/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@Valid Supplier supplier, BindingResult result) {
        if (result.hasErrors()) {
            return "supplier/edit";
        }
        supplierDao.save(supplier);
        return "redirect:/supplier/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model) {
        Supplier supplier = supplierDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Supplier:" + id));
        supplierDao.delete(supplier);
        return "redirect:/supplier/";
    }

}
