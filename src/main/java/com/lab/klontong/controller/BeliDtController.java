package com.lab.klontong.controller;

import com.lab.klontong.dao.BeliDao;
import com.lab.klontong.dao.BeliDtDao;
import com.lab.klontong.entity.Beli;
import com.lab.klontong.entity.BeliDt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/beliDt")
public class BeliDtController {
    @Autowired
    BeliDtDao beliDtDao;
    @Autowired
    BeliDao beliDao;

    @GetMapping("/create")
    public String create(BeliDt beliDt, HttpServletRequest request, Model model) {
        String beliId = request.getParameter("beliId").toString();
        Beli beli = beliDao.findById(beliId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + beliId));
        model.addAttribute("beli", beli);
        return "/beliDt/create";
    }

    @PostMapping("/save")
    public String save(HttpServletRequest request, @Valid BeliDt beliDt, BindingResult result, Model model) {
        String beliId = request.getParameter("beliId").toString();
        Beli beli = beliDao.findById(beliId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + beliId));

        if (beli.getStatus() != Beli.Status.OPEN) {
            result.addError(new ObjectError("beliId", "Hanya bisa mengubah data dengan status OPEN"));
        }

        if (result.hasErrors()) {
            model.addAttribute("beli", beli);
            return "/beliDt/create";
        }

        beliDt.setBeli(beli);
        beliDtDao.save(beliDt);

        return "redirect:/beliDt/create?beliId=" + beliDt.getBeli().getId();
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        BeliDt beliDt = beliDtDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Detail Pembelian:" + id));

        model.addAttribute("beli", beliDt.getBeli());
        model.addAttribute("beliDt", beliDt);
        return "beliDt/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@Valid BeliDt beliDt, BindingResult result, Model model) {
        // beliDt.getBeli() = null, tidak bisa dipercaya karena dari form user
        String id = beliDt.getId();
        BeliDt beliDtOri = beliDtDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Detail Pembelian:" + id));

        Beli beli = beliDtOri.getBeli();
        if (beli.getStatus() != Beli.Status.OPEN) {
            result.addError(new ObjectError("beliId", "Hanya bisa mengubah data dengan status OPEN"));
        }
        if (result.hasErrors()) {
            model.addAttribute("beli", beli);
            model.addAttribute("beliDt", beliDt);
            return "beliDt/edit";
        }
        beliDt.setBeli(beliDtOri.getBeli());
        beliDtDao.save(beliDt);
        return "redirect:/beliDt/create?beliId=" + beli.getId();
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id, Model model) {
        BeliDt beliDt = beliDtDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Detail Pembelian:" + id));

        Beli beli = beliDt.getBeli();

        if (beli.getStatus() != Beli.Status.OPEN) {
            model.addAttribute("errorMessage", "Hanya bisa menghapus data dengan status OPEN");
            return "page/error";
        }

        beliDtDao.delete(beliDt);
        return "redirect:/beliDt/create?beliId=" + beli.getId();
    }

}
