package com.lab.klontong.controller;

import com.lab.klontong.dao.BarangDao;
import com.lab.klontong.dao.BeliDao;
import com.lab.klontong.entity.Barang;
import com.lab.klontong.entity.Beli;
import com.lab.klontong.exception.KlontongException;
import com.lab.klontong.service.BeliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.SQLException;

@Controller
@RequestMapping("/beli")
public class BeliController {
    @Autowired
    BarangDao barangDao;
    @Autowired
    BeliDao beliDao;

    @Autowired
    BeliService beliService;

    @GetMapping("/")
    public String index(Model model, @SortDefault("tanggal")  Pageable pageable) {
        model.addAttribute("belis", beliDao.findAll(pageable));
        return "beli/index";
    }

    @GetMapping("/create")
    public String create(Beli beli) {
        return("beli/create");
    }

    @PostMapping("/save")
    public String save(HttpServletRequest request, @Valid Beli beli, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "beli/create";
        }
        beliDao.save(beli);
        return "redirect:/beli/";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Beli beli = beliDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + id));

        model.addAttribute("beli", beli);
        return "beli/edit";
    }

    @PostMapping("/update/{id}")
    public String update(@Valid Beli beliNew, BindingResult result) {
        // load status dari database
        String id = beliNew.getId();
        Beli beli = beliDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + id));
        if (beli.getStatus() != Beli.Status.OPEN) {
            result.addError(new ObjectError("beli", "Hanya bisa mengubah data dengan status OPEN"));
        }
        beliNew.setStatus(beli.getStatus());

        if (result.hasErrors()) {
            return "beli/edit";
        }
        beliDao.save(beliNew);
        return "redirect:/beli/";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id, Model model) {
        Beli beli = beliDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + id));

        if (beli.getStatus() != Beli.Status.OPEN) {
            model.addAttribute("errorMessage", "Hanya bisa menghapus data dengan status OPEN");
            return "page/error";
        }

        beliDao.delete(beli);
        return "redirect:/beli/";
    }

    @GetMapping("/closing")
    public String closing(HttpServletRequest request, Model model) {
        String id = request.getParameter("beliId").toString();
        Beli beli = beliDao.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + id));

        try {
            beliService.closing(beli);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (KlontongException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "page/error";
        }

        return "redirect:/beli/";
    }

}
