package com.lab.klontong.service;

import com.lab.klontong.dao.BarangDao;
import com.lab.klontong.dao.BeliDao;
import com.lab.klontong.entity.Barang;
import com.lab.klontong.entity.Beli;
import com.lab.klontong.exception.KlontongException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLException;

@SpringBootTest
public class BeliServiceTests {
    @Autowired BarangDao barangDao;
    @Autowired BeliDao beliDao;
    @Autowired BeliService beliService;

    @Test
    void closingTest() {
        long idMieSedap = 2;
        Barang mieSedap = barangDao.findById(idMieSedap)
                .orElseThrow(() -> new IllegalArgumentException("mie sedaapp tidak ditemukan"));
        double stokAwal = mieSedap.getStok();

        String idPembelian = "994d0f85-28a5-4153-a3a3-62ce08f04488";
        Beli beli = beliDao.findById(idPembelian)
                .orElseThrow(() -> new IllegalArgumentException("Invalid ID Transaksi Pembelian:" + idPembelian));
        try {
            beliService.closing(beli);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (KlontongException e) {
            throw new RuntimeException(e);
        }

        mieSedap = barangDao.findById(idMieSedap)
                .orElseThrow(() -> new IllegalArgumentException("mie sedaapp tidak ditemukan"));
        double stokAkhir = mieSedap.getStok();
        boolean isStokBertambah = stokAkhir > stokAwal;
        Assertions.assertEquals(true, isStokBertambah);
    }
}
