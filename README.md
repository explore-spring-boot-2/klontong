# Klontong

![Penjualan](./readme/img/ss-penjualan.png "Penjualan")

## Deskripsi
Aplikasi sederhana untuk toko klontong.

## Menu
- Master
    - Barang
    - Vendor

- Transaksi
    - Beli
    - Jual
	
- Laporan
    - Daftar Barang
    
## Dependencies
- Thymeleaf (template engine)
- H2 Database (RDBMS)
- Flyway DB (database-migration tool)
- Jasper report (reporting tool)
- WebJars AdminLTE (client-side web libraries)
- Spring Data JPA (Java Persistence API)